import React from 'react';
import App from './components/App';
import List from './components/containers/Home/List';
import Movie from './components/containers/Movie';
import listData from '../test/listData.json';
import movieData from '../test/movieData.json';

// describe('<App />', () => {
//   it('renders the App wrapper', () => {
//     const wrapper = mount(<App />);
//     expect(wrapper.find('#appConatiner')).to.have.lengthOf(1);
//   });
// });

describe('<List />', () => {
  it('renders the mobile site List', () => {
    const wrapper = mount(<List data={listData}/>);
    wrapper.setState({ start: 0,end:2 });
    expect(wrapper.find('li')).to.have.lengthOf(2);
  });

  it('renders the tab site List', () => {
    const wrapper = mount(<List data={listData}/>);
    wrapper.setState({ start: 0,end:4 });
    expect(wrapper.find('li')).to.have.lengthOf(4);
  });

  it('renders the desktop site List', () => {
    const wrapper = mount(<List data={listData}/>);
    wrapper.setState({ start: 0,end:6 });
    expect(wrapper.find('li')).to.have.lengthOf(6);
  });
});

describe('<Movie />', () => {
  it('renders the movie detail page', () => {
    const wrapper = mount(<Movie />);
    wrapper.setState({ data: movieData });
    expect(wrapper.find('#movieConatiner')).to.have.lengthOf(1);
  });
});

describe('<VideoPlayer />', () => {
  it('renders the video player', () => {
    const wrapper = mount(<Movie />);
    wrapper.setState({ showVideo: true });
    expect(wrapper.find('VideoPlayer')).to.have.lengthOf(1);
  });
});