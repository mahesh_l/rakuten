const API_HOST = 'https://gizmo.rakuten.tv/v3';
const API_DEFAULT_PARAMS = 'classification_id=5&device_identifier=web&locale=es&market_code=es';
const LISTS = 'estrenos-imprescindibles-en-taquilla';

module.exports = Object.assign({
  API_HOST,
  API_DEFAULT_PARAMS,
  LISTS
});
