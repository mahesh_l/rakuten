import axios from 'axios';

export default class FetchHelper {

	request(method, params) {

		let apiHeaders = {
			'Content-Type': (params.isJson)?'text/json;application/json':'application/x-www-form-urlencoded;charset=UTF-8'
		};
		let options = {
			method: (method)? method: 'get',
			url: params.url,
			headers: apiHeaders,
			withCredentials: (params.includeCredentials) ? params.includeCredentials : false
		};

		if (params.data){
			options.data = params.data;
		}
		
		return this.apiCall(options);
	}

	apiCall(options) {
		return axios(options);
	}
}