import React from 'react';
import Header from './presentational/common/Header';
import Main from './Main';
import Footer from './presentational/common/Footer';

const App = () => (
  <div id="appConatiner">
    <Header />
    <Main />
    <Footer />
  </div>
);

export default App;
