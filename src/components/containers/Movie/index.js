import React, { Component } from 'react';
import lozad from 'lozad';

import { FaStar, FaUserAlt, FaPlayCircle, FaThumbTack , FaMapPin} from 'react-icons/fa';

import apiCalls from '../../../utils/FetchHelper.js';
import Constants from '../../../constants/constant.js';
import Loader from '../../presentational/Common/Loader';
import NoMovie from '../../presentational/Common/Images/noMovie.png';
import SocialMedia from '../../presentational/SocialMedia/';
import MovieDetails from '../../presentational/MovieDetails/';
import VideoPlayer from '../../presentational/VideoPlayer/';

import './index.css';

const observer = lozad();

class Movie extends Component {

	constructor(props){
		super(props);
		this.state = {
			data: {},
			counter: false,
			showVideo: false
		};
		this.apiHelper = new apiCalls();
	}

	componentDidMount(){
		const url = Constants.API_HOST+location.pathname+'?'+Constants.API_DEFAULT_PARAMS;
        let params = {
    			url: url
    		};
    	let getListData = this.apiHelper.request('get', params);
    		getListData.then((response) => {
            if(response.status == 200){
            	var data = response.data.data;
                this.setState({
                    data: data,
                    counter: true
                });
            }
        }).then(()=>{
            this.lazyloadImages();
        });	
	}
    
    playVideo = () =>{
        this.setState({
        	showVideo: true
        });
	}

	lazyloadImages(){
        observer.observe();
    }
    stopVideo = () =>{
    	this.setState({
        	showVideo: false
        });
    }

	render(props,state){
		const data = this.state.data;
		const actors = data.actors;
		const awards = data.awards;
		const directors = data.directors;
		const reviews = data.user_reviews;

		let heroBanner = data && data.images && data.images.artwork && data.images.artwork || '';
		let score = '',
		    vote = '',
		    price = 0,
		    priceString = '',
		    age,
		    country,
		    languageAndSubtitle;
		
		if(data.scores){
            data.scores.map(i=>{
            	if(i.highlighted == true){
            		score = i.score;
            		vote = i.formatted_amount_of_votes;
            		return;
            	}
            });
		}

		if(data.order_options){
			const orderOptions = data.order_options;
			const n = orderOptions.length;
			price = orderOptions[0].points.cost;
			priceString = orderOptions[0].price;
            for(var i=1; i < n; i++){
            	if(orderOptions[i].points.cost < price){
            		price = orderOptions[i].points.cost;
            		priceString = orderOptions[i].price;
            	}
            }
		}

        if(data.classification){
        	age = data.classification.age;
        }
        if(data.countries){
        	country = data.countries[0].name;
        }
        if(data.view_options && data.view_options.public && data.view_options.public.trailers){
        	languageAndSubtitle = data.view_options.public.trailers;
        }

		return (
			<div className='container'>
			    {!this.state.showVideo && data && 
                   	<div id="movieConatiner">
                   		<div className="hero-banner" style={{backgroundImage:`url(${heroBanner})`}}>
                   			<div className="detail-wrap">
                   				<div className="top-buttons">
                   					<span className="ban-btn">
	                   					<span onClick={()=>{this.playVideo()}} className="bg-round playIcon"><FaPlayCircle /></span>
	                   					<span className="bg-round-text">Trailer</span>
	                   				</span>
                   					<span className="ban-btn">
                   						<span className="bg-round playIcon"><FaMapPin /></span>
                   						<span className="bg-round-text">Add to I want to see</span>
                   					</span>
                   				</div>
	                   			<div className="banner-detail">
	                   				<p>
	                   					<FaStar className='font-yellow' />
	                   					<span className='font-white'> &nbsp;{score}</span>
	                   					&nbsp;&nbsp;
	                   					<FaUserAlt className='font-white' />
	                   					<span className='font-white'>&nbsp;{vote}</span>
	                   				</p>
		                   			<h1>{data.original_title}</h1>
		                   			<div className="action-btns">
		                   				<button>Watch now <span> from {priceString}</span> or <span>{price}p</span></button>
		                   				<button className="btn-trans">Redeem Coupon</button>
		                   			</div>
		                   		</div>
		                   	</div>
                   		</div>
                   		<div className="cont-wrap">
                   			<div className="cont">
                   				<MovieDetails 
                   				age={age} 
                   				duration={data.duration} 
                   				year={data.year} country={country} 
                   				originalTitle={data.original_title} 
                   				description={data.plot} 
                   				languageAndSubtitle={languageAndSubtitle}
                   				directors={data.directors}
                   				scores={data.scores}
                   				genres={data.genres}

                   				/>
                   				<SocialMedia />
                   			</div>
                   		</div>
                   </div>
			    }
			    {this.state.showVideo && <VideoPlayer contentId={data.id} title={data.original_title} returnUrl={location.pathname} hideVideo={this.stopVideo}/>}
				{!this.state.counter && <Loader />}
			</div>
		)
	}
}

export default Movie;