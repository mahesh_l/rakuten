import React from 'react';

import NotFound from './images/noresult.jpeg';
//import './index.css';

const NoResult = () => (
	 <div className="no-results">
	    <div className="no-results-inner">
	        <img src={NotFound} alt="noResult" />
	    </div>
	</div>
);

export default NoResult;