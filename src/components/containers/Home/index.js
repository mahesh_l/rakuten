import React, { Component } from 'react';

import MoviesList from './List';
import NoResult from './NoResult';

import apiCalls from '../../../utils/FetchHelper.js';
import Constants from '../../../constants/constant.js';
import Loader from '../../presentational/Common/Loader';

class Home extends Component {

	constructor(props){
		super(props);
		this.state = {
			data: {},
			nMovies: false,
			counter: false
		};
        this.apiHelper = new apiCalls();
	}

	componentDidMount(){
		const url = Constants.API_HOST+'/lists/'+Constants.LISTS+'?'+Constants.API_DEFAULT_PARAMS;
        let params = {
    			url: url
    		};
    	let getListData = this.apiHelper.request('get', params);
    		getListData.then((response) => {
            if(response.status == 200){
            	var data = response.data.data;
            	var nMovies = (data ? (data.contents? (data.contents.meta?data.contents.meta.pagination.count:0):0 ):0);
                this.setState({
                    data: data,
                    nMovies: nMovies,
                    counter: true
                });
            }
        });	
	}

	render(){
        let state = this.state;
		return (
			<div>
			{state.nMovies > 0 && <MoviesList data={state.data} nMovies={state.nMovies} listId={Constants.LISTS} />}
			{state.nMovies === 0 && <NoResult />}
			{!state.counter && <Loader />}
			</div>
		);
	}
}

export default Home;