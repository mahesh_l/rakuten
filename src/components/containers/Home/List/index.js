import React, { Component } from 'react';
import Waypoint from 'react-waypoint';

import MovieTupple from '../../../presentational/MovieTupple';
import './index.css';

const windowWidth = window.innerWidth;

class List extends Component {

	constructor(props){
		super(props);
		this.state = {
            start: 0,
            end: this.getDefaultLength() 
		};
	}
    getDefaultLength(){
    	if (windowWidth < 321) {
			return 2;
		} else if (windowWidth < 731) {
			return 4;
		} else {
			return 6;
		}
    }
	loadMovieTupple (data){
        let htmlElement = [];
		let viewList;
		let start = this.state.start;
		let end = this.state.end;
        viewList = data.slice(start, end);
        viewList.map(i=>{
       	i.href = '/'+i.type+'/'+i.id;
       	 htmlElement.push(<MovieTupple key={i.numerical_id}  data={i}/>);
       });
       return htmlElement;
	}
	handleArrowClick = (n) =>{
        this.setState({
        	start: this.state.start+n,
        	end: this.state.end+n
        });
	}

	render(){
		var data = this.props.data;
		var title = data.name;
		var moviesList = data.contents.data;
		var listLength = moviesList.length;
		return (
			<div className="container">
			    <h2 className='font-white'>{title}</h2>
				<div className='moviesListPanel'>
					<ul id='moviesList'>
						{this.loadMovieTupple(moviesList)}
					</ul>
					<div id='sliderControls'>
						{this.state.start > 0 && <p className='moveLeft pointer' onClick={() => { this.handleArrowClick(-1)}}>&lt;</p>}
						{this.state.end < listLength &&  <p className='moveRight pointer' onClick={() => { this.handleArrowClick(1) }}>&gt;</p>}
					</div>
				</div>
			</div>
		)
	}
}

export default List;