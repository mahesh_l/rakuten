import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
import { FaStar, FaUserAlt } from 'react-icons/fa';
import lozad from 'lozad';

import NoMovie from  '../../presentational/Common/Images/noMovie.png';
import './index.css';

const observer = lozad(); // lazy loads elements with default selector as '.lozad'

class MovieTupple extends Component {

	componentDidMount(){
		this.lazyloadImages();
	}

	lazyloadImages(){
        observer.observe();
    }

	render(props){
		var data = this.props.data;
		return (
			<li className="itemContainer">
			    <a href={data.href}>
				    <picture>
				        <img className="lozad" data-src={data.images.artwork} title={data.title} alt={data.title} src={NoMovie}/>
				    </picture>
				    <div className='movieBrief'>
				    	<h5 className="item-title font-white">{data.title}</h5>
						<p className='ratingPanel'>
							<span className='font-yellow'><FaStar /></span>
							<span className='font-yellow'>&nbsp;{data.highlighted_score.score}</span>
							&nbsp;&nbsp;
							<span className='font-white userIcon'><FaUserAlt /></span>
							<span className='font-white'>&nbsp;{data.highlighted_score.formatted_amount_of_votes}</span>
						</p>
				    </div>
			    </a>
			</li>
		);
	}
}

export default MovieTupple;