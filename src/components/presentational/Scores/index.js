import React, {Component} from 'react';
import { FaStar, FaUser } from 'react-icons/fa';

import './index.css';

export default class Scores extends Component{
	render(){
		const data = this.props.data;
		return(
			<div className="director scores">
				<h5 className="heading"><FaStar />&nbsp;Scores</h5>
				<ul>
					
					{data && 
					data.map(i=>
						<li key={i.id}>
							<div className='genreIcon'>
							    <span className="score"><strong>{i.score}</strong></span><br/>
							    <span className="userVotes"><FaUser /> {i.formatted_amount_of_votes}</span>
							</div>
							<span className="caption">{i.site.name}</span>
						</li>
					)
				}	
				</ul>
			</div>
		)
	}
}