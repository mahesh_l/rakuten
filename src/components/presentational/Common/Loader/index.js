import React from 'react';

import LoaderImg from '../Images/loading-indicator.gif';

import './index.css';

const Loader = () => (
  <center>
      <img id="loaderImg" src={LoaderImg} alt="fetching data" title="fetching data"/>
  </center>
);

export default Loader;