import React from 'react';

import './index.css';

const Footer = () => (
  <footer>
    <p id="footer">Copyright @ 2019 RakutenTv</p>
  </footer>
);

export default Footer;