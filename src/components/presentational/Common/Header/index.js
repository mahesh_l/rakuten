import React from 'react';
import { Link } from 'react-router-dom';

import Logo from './images/logo.png';
import './index.css';

const Header = () => (
  <header>
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark static-top">
	  <div className="container">
	    <Link className="navbar-brand" to="/">
	          <img src={Logo} alt="" />
	        </Link>
	  </div>
	</nav>
  </header>
);

export default Header;
