import React, {Component} from 'react';
import { FaUser, FaUsersCog } from 'react-icons/fa';
import './index.css';

export default class Genres extends Component{
	render(){
		const data = this.props.data;
		return(
			<div className="director genres">
				<h5 className="heading"><FaUsersCog />&nbsp;Genres</h5>
				<ul>
					
					{data && 
					data.map(i=>
						<li key={i.id}>
							<span className='genreIcons'><FaUser /></span>
							<span className="caption">{i.name}</span>
						</li>
					)
				}	
				</ul>
			</div>
		)
	}
}