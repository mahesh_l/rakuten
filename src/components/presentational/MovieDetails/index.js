import React, {Component} from 'react';
import { FaUser, FaFlag, FaUserFriends, FaClock, FaCalendar, FaInfoCircle} from 'react-icons/fa';

import Director from '../Director';
import Scores from '../Scores';
import Specs from '../Specs';
import Genres from '../Genres';
import './index.css';

export default class MovieDetails extends Component{
	render(){
		const data = this.props;
		return(
			<div className="main-cont">
				<div className="main-cont-inner">
					<FaUserFriends />
					&nbsp;<span>{data.age}</span>
					&nbsp;&nbsp;
					<FaClock />
					&nbsp;<span>{data.duration} minutes</span>
					&nbsp;&nbsp;
					<FaCalendar />
					&nbsp;<span>{data.year}</span>
					&nbsp;&nbsp;
					<FaFlag />
					&nbsp;<span>{data.country}</span>
					&nbsp;&nbsp;
					<FaInfoCircle />
					&nbsp;<span>Original title: {data.originalTitle}</span>
					<p className="description">{data.description}</p>
					{data.languageAndSubtitle && <Specs languageAndSubtitle={data.languageAndSubtitle}/>}
					<Director data={data.directors}/>
					<Scores data={data.scores}/>
					<Genres data={data.genres}/>
				</div>
			</div>
		)
	}
}