import React, {Component} from 'react';
import { FaCommentDots} from 'react-icons/fa';
import './index.css';

export default class Specs extends Component{

	loadAudio(data){
         let htmlAudio = [];
         let n = 1;
         data.map(i=>{
             i.audio_languages.map(j=>{
	             htmlAudio.push(<li key={j.id+'-'+n}>{j.name}</li>);
	         });
	         n++;
         });
         return htmlAudio;
	}
	loadSubTitles(data){
         let htmlAudio = [];
         let n = 1;
         data.map(i=>{
             i.subtitle_languages.map(j=>{
	             htmlAudio.push(<li key={j.id+'-'+n}>{j.name}</li>);
	         });
	         n++;
         });
         return htmlAudio;
	}
	render(){
		const data = this.props.languageAndSubtitle;
		let audio,
		    subTitle;
		
		return(
			<div className="specs">
				<h5 className="heading"><FaCommentDots />Languages ​​and subtitles</h5>
				{ data && 
					<div className='audioAndSubtitleCategory'>
						<div className='audioCategory'>
						    <h6>Audio</h6>
						    <ul className='listStyleNone'>
						       {this.loadAudio(data)}
						   </ul>
						</div>
						<div className='subtitleCategory'>
						    <h6>Subtitles</h6>
						    <ul className='listStyleNone'>
						       {this.loadSubTitles(data)}
						   </ul>
						</div>
					</div>
				}
			 </div>
		)
	}
}