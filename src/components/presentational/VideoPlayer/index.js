import React, { Component } from 'react';
import { FaAngleLeft } from 'react-icons/fa';

import apiCalls from '../../../utils/FetchHelper.js';
import Constants from '../../../constants/constant.js';
import Loader from '../../presentational/Common/Loader';

import './index.css';

class VideoPlayer extends Component {

	constructor(props){
		super(props);
		this.state = {
			videoUrl: '',
			counter: true
		};
		this.apiHelper = new apiCalls();
	}

	componentDidMount(){
        const url = Constants.API_HOST+'/me/streamings?'+Constants.API_DEFAULT_PARAMS;
        let data = new URLSearchParams();
        data.append('audio_language', "SPA");
        data.append('audio_quality', "2.0");
        data.append('content_id', this.props.contentId);
        data.append('content_type', "movies");
        data.append('device_serial', "device_serial_1");
        data.append('device_stream_video_quality', "FHD");
        data.append('player', "web:PD-NONE");
        data.append('subtitle_language', "MIS");
        data.append('video_type', "trailer");
   
        let params = {
    			url: url,
    			data: data
    		};
    	let getListData = this.apiHelper.request('post', params);
    		getListData.then((response) => {
            if(response.status == 200){
            	var data = response.data.data;
                this.setState({
                    videoUrl: data.stream_infos[0].url,
                    counter: false
                });
            }
        });	
	}
    
	handleBackClick = () => {
		this.props.hideVideo();
	}

	render(){
		return (
			<div>
			{!this.state.counter && 
               <div id='videoPlayerPane'>
                    <p className="videoTitle" onClick={()=>{this.handleBackClick()}}><FaAngleLeft />{this.props.title}</p>
	                <video autoPlay controls muted id='trailerPlayer'>
	                    <source src={this.state.videoUrl} />
	                    Your browser does not support HTML5 video.
	            	</video>
	            </div>
			}
		    {this.state.counter && <Loader />}
		    </div>
		)
	}
}

export default VideoPlayer;