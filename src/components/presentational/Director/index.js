import React, {Component} from 'react';
import { FaCamera } from 'react-icons/fa';
import Loading from '../Common/Images/loading-indicator.gif';
import './index.css';

export default class Director extends Component{

	render(){
		var data = this.props.data;
		return(
			<div className="director">
				<h5 className="heading"><FaCamera />&nbsp;Direction and distribution</h5>
				<ul>
				{data && 
					data.map(i=>
						<li key={i.id}>
							<img  src={i.photo} alt={i.name}/>
							<span className="caption">{i.name}</span>
						</li>
					)
				}	
				</ul>
			</div>
		)
	}
}