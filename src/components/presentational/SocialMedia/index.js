import React, { Component } from 'react';
import { FaReadme, FaTwitter, FaFacebook, FaPinterest, FaGooglePlus} from 'react-icons/fa';

import './index.css';

export default class SocialMedia extends Component {
	render(){
		return(
			<div className="side-cont social-cont">
				<div className="social-icons">
					<ul>
						<li>
							<span className="s-icon"><FaReadme /></span>
							<span className="s-txt">Mark as seen</span>
						</li>
						<li>
							<span className="s-icon"><FaTwitter /></span>
							<span className="s-txt">Twitter</span>
						</li>
						<li>
							<span className="s-icon"><FaFacebook /></span>
							<span className="s-txt">Facebook</span>
						</li>
						<li>
							<span className="s-icon"><FaPinterest /></span>
							<span className="s-txt">Pinterest</span>
						</li>
						<li>
							<span className="s-icon"><FaGooglePlus /></span>
							<span className="s-txt">Google+</span>
						</li>
					</ul>
				</div>
			</div>
		)
	}
}