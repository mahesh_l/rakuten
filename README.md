# RakutenTv

Steps to run the application:-

1. Clone the the repo
2. cd rakuten/
3. Run the following commands
   npm install
   npm run build
   npm start (Local server)
   
Note: Please make sure you have enabled CORS exception plugin in your browser.   
