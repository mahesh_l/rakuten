const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './src/index.js',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test:/\.(css|scss)$/,
        use:['style-loader', 'css-loader']
       
      },{
        test: /\.(jpe?g|png|gif|mp3)$/i,
        use: ['file-loader']
      }
    ]
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].bundle.js',
    publicPath: path.resolve(__dirname, 'dist')
  },
  plugins:[
	  new HtmlWebpackPlugin({
	  	template: './src/index.html'
	  })
  ],
  devServer: {
    contentBase: './dist',
    historyApiFallback: true
  }
};